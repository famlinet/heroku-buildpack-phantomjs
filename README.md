# Heroku-Buildpack-PhantomJS

## Purpose

To install PhantomJS in the Heroku app.

## Buildpack operations
*	Download and extract a binary for PhantomJS.
*	Create a `/bin` directory in the app's root directory.
*	Install the extracted binary in the new directory.

## Public repo

This is a public repository, as Heroku does not support accessing private repositories (requiring authentication) to clone buildpacks.
